const USERS_URL = 'https://jsonplaceholder.typicode.com/users';
const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts';

const getUsers = async () => {
    const {data: users} = await axios.get(USERS_URL);
    return users;
};

const getPosts = async () => {
    const {data: posts} = await axios.get(POSTS_URL);
    return posts;
};

(function ($) {
    var carouselBody = $('.carousel-inner');
    var carouselIndicatorsBody = $('.carousel-indicators');

    $(document).ready(async function () {
        const users = await getUsers();
        const posts = await getPosts();

        const items = users.map((user, index) => {
            return {...user, post: posts[index]};
        });

        buildCarousel(items);
    });

    function buildCarousel(items) {
        const carouselItems = items.map((i, idx) => buildCarouselItem(i, idx));
        const carouselIndicators = items.map((i, idx) => buildCarouselIndicator(i, idx));

        carouselBody.html(carouselItems.join(''));
        carouselIndicatorsBody.html(carouselIndicators.join(''));

        $('.carousel').carousel();
    }

    function buildCarouselIndicator(i, index) {
        return `<li data-target="#carouselExampleIndicators" data-slide-to="${index}" class="carousel-indicator ${index === 0 ? 'active' : ''}"></li>`
    }

    function buildCarouselItem(item, index) {
        return `
            <div class="carousel-item ${index === 0 ? 'active' : ''} text-center">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 text-center">
                        <img class="testimonials--image mt-5 mb-4" src="img/person_${getImageId(index, 4)}.jpg" alt="${item.name}">
    
                        <p class="font-italic">"${item.post.body}"</p>
    
                        <p class="testimonials--name">${item.name}</p>
                    </div>
                </div>
            </div>
        `;
    }

    /**
     * Get a random index to use placeholder images
     *
     * @param index
     * @param limit
     * @returns {number}
     */
    function getImageId(index, limit) {
        return Math.floor(Math.random() * limit) + 1;
    }

    /**
     * Easy scroll to the different landing sections
     */
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $(document).scroll(function () {

    });
})($);